//
//  TreggiOSSignInTests.m
//  TreggiOS
//
//  Created by m3nTe on 8/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <AFNetworking/AFHTTPClient.h>
#import "TreggConfig.h"
#import "TreggUser.h"

@interface TreggiOSSignInTests : XCTestCase

@end

@implementation TreggiOSSignInTests

- (void)setUp
{
    [super setUp];

    NSLog(@"TreggiOSSignInTests setUp");
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = [userDefaults dictionaryRepresentation];
    for (id key in dict) {
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testSignInWithSuccesfullWithUsernameAndPassword
{
    id mockClient = [OCMockObject mockForClass:[TreggClientAPI class]];
    
    [[[mockClient expect] andDo:^(NSInvocation *invocation) {

        // sucess block:
        void (^successBlock)(AFHTTPRequestOperation *operation, id responseObject);
        
        // Using NSInvocation, we get access to the concrete block function
        // that has been passed in by the actual test
        // the arguments for the actual method start with 2 (see NSInvocation doc)
        [invocation getArgument:&successBlock atIndex:4];
        
        // now we invoke the successBlock with some "JSON"...:
        successBlock(nil, [NSDictionary dictionaryWithObjectsAndKeys:@"token_123", @"access_token", [NSNumber numberWithInt:1], @"user[id]", @"antonio_parisi", @"user[username]", @"password", @"user[password]", nil]);
        
    }] postPath:[OCMArg any] parameters:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    
    [TreggConfig setClientId:@"321345231"];
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc] init];
    [dictParams setValue:[TreggConfig clientId] forKey:@"client_id"];
    [dictParams setValue:@"password" forKey:@"grant_type"];
    [dictParams setValue:@"antonio_parisi" forKey:@"username"];
    [dictParams setValue:@"password_value" forKey:@"password"];
    
    [mockClient postPath:kTFLoginPath parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *response = responseObject;
        
        // access_token verification
        XCTAssertEqual(@"token_123", [response valueForKey:@"access_token"], @"Access token is not the same");
        
        // save token
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:[response valueForKey:@"access_token"] forKey:kTFUserAccessToken];
        [userDefaults synchronize];
        
        XCTAssertEqual(@"token_123", [userDefaults objectForKey:kTFUserAccessToken], @"Saved access_token is not the same");
        
        // save user
        NSMutableDictionary *dictTreggUser = [[NSMutableDictionary alloc] init];
        
        [dictTreggUser setValue:[response valueForKey:@"user[id]"] forKey:@"id"];
        [dictTreggUser setValue:[response valueForKey:@"user[username]"] forKey:@"username"];
        [dictTreggUser setValue:[response valueForKey:@"user[password]"] forKey:@"password"];
        
        [userDefaults setValue:dictTreggUser forKey:kTFUserObjectKey];
        [userDefaults synchronize];
        
        XCTAssertEqual([dictTreggUser count], [[[NSDictionary alloc] initWithDictionary:[userDefaults objectForKey:kTFUserObjectKey]] count], @"Saved user is not the same");
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        XCTFail(@"Error during signUp");
    }];

}

- (void)testSignInWithPasswordMismatch
{
    id mockClient = [OCMockObject mockForClass:[TreggClientAPI class]];
    
    [[mockClient expect] postPath:kTFLoginPath parameters:[OCMArg any] success:nil failure:[OCMArg checkWithBlock:^BOOL(void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error)) {
        failureBlock(nil, [NSError errorWithDomain:@"com.fungostudios.TreggiOS" code:400 userInfo:@{ NSLocalizedDescriptionKey : @"Password is mismatched"}]);
        return YES;
    }]];
    
    [TreggConfig setClientId:@"321345231"];
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc] init];
    [dictParams setValue:[TreggConfig clientId] forKey:@"client_id"];
    [dictParams setValue:@"password" forKey:@"grant_type"];
    [dictParams setValue:@"antonio_parisi" forKey:@"username"];
    [dictParams setValue:@"wrong_password" forKey:@"password"];
    
    [mockClient postPath:kTFLoginPath parameters:dictParams success:nil
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    XCTAssertEqual(400, error.code, @"Error's code is not the same.");
                    XCTAssertEqual(@"Password is mismatched", error.localizedDescription, @"Error's description is not the same.");
                }];
}

- (void)testSignInUserAlreadyLoggedIn
{
    TreggUser *treggUser = [TreggUser currentUser];
    XCTAssertNil(treggUser, @"Current user is not nil");
    
    // mock response after a succesfull signIn
    NSDictionary *dictResponse = [NSDictionary dictionaryWithObjectsAndKeys:@"token_123", @"access_token", [NSNumber numberWithInt:1], @"user[id]", @"antonio_parisi", @"user[username]", @"password_value", @"user[password]", nil];

    // serialize treggUser
    treggUser = [[TreggUser alloc] init];
    
    NSNumber *treggUserId = [NSNumber numberWithInt:[[dictResponse valueForKey:@"user[id]"] intValue]];
    NSString *treggUserUserName = [dictResponse valueForKey:@"user[username]"];
    
    [treggUser setUniversalId:treggUserId];
    [treggUser setUserName:treggUserUserName];
    
    // deserialize treggUser
    
    NSMutableDictionary *dictTreggUser = [[NSMutableDictionary alloc] init];
    
    [dictTreggUser setValue:treggUser.universalId forKey:@"id"];
    [dictTreggUser setValue:treggUser.userName forKey:@"username"];
    [dictTreggUser setValue:treggUser.facebookUid forKey:@"facebook_uid"];
    
    NSData *archivedTreggUser = [NSKeyedArchiver archivedDataWithRootObject:dictTreggUser];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:archivedTreggUser forKey:kTFUserObjectKey];
    [userDefaults synchronize];
    
    treggUser = [TreggUser currentUser];
    XCTAssertNotNil(treggUser, @"Current user is nil");

    // Calling signIn and return warning message 'user already logged in'
    
//    TreggUser *treggUser = [[TreggUser alloc] init];
//    id mockSignIn = [OCMockObject partialMockForObject:treggUser];
//    
//    BOOL test = true;
//    [[[mockSignIn stub] andReturnValue:OCMOCK_VALUE(test)] ];
//    
//    [[mockClient expect] postPath:kTFLoginPath parameters:[OCMArg any] success:nil failure:[OCMArg checkWithBlock:^BOOL(void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error)) {
//        failureBlock(nil, [NSError errorWithDomain:@"com.fungostudios.TreggiOS" code:400 userInfo:@{ NSLocalizedDescriptionKey : @"User already logged in"}]);
//        return YES;
//    }]];
//    
//    [TreggConfig setClientId:@"321345231"];
//    
//    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc] init];
//    [dictParams setValue:[TreggConfig clientId] forKey:@"client_id"];
//    [dictParams setValue:@"password" forKey:@"grant_type"];
//    [dictParams setValue:@"antonio_parisi" forKey:@"username"];
//    [dictParams setValue:@"password_value" forKey:@"password"];
//    
//    [mockClient postPath:kTFLoginPath parameters:dictParams success:nil failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        XCTAssertEqual(400, error.code, @"Error's code is not the same.");
//        XCTAssertEqual(@"User already logged in", error.localizedDescription, @"Error's description is not the same.");
//    }];

//    OCMockObject *blah = [OCMockObject mockForClass:[UIAlertView class]];
//    OCMockObject *UIAlertViewMock = [[[blah stub] andReturn:[[UIAlertView alloc] initWithTitle:@"Call Security" message:@"(000)-000-0000" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil]] initWithTitle:@"Call Security" message:@"(000)-000-0000" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
//    [[UIAlertViewMock expect] show];
//    // we should display UIAlertView on a ViewController
//    [UIAlertViewMock verify];
    
}

@end
