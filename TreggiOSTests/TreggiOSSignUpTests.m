//
//  TreggiOSSignUpTests.m
//  TreggiOSSignUpTests
//
//  Created by m3nTe on 4/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <AFNetworking/AFNetworking.h>
#import "TreggUser.h"

@class TreggUser;

@interface TreggiOSSignUpTests : XCTestCase

@end

@implementation TreggiOSSignUpTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testSignUpWithSuccesfull
{
    id mockClient = [OCMockObject mockForClass:[AFHTTPClient class]];
    
    [[[mockClient expect] andDo:^(NSInvocation *invocation) {
        
        // sucess block:
        void (^successBlock)(AFHTTPRequestOperation *operation, id responseObject);
        
        // Using NSInvocation, we get access to the concrete block function
        // that has been passed in by the actual test
        // the arguments for the actual method start with 2 (see NSInvocation doc)
        [invocation getArgument:&successBlock atIndex:4];
        
        // now we invoke the successBlock with some "JSON"...:
        successBlock(nil, [NSDictionary dictionaryWithObjectsAndKeys:@"token_123", @"access_token", [NSNumber numberWithInt:1], @"user[id]", @"antonio_parisi", @"user[username]", @"password", @"user[password]", nil]);
        
    }] postPath:[OCMArg any] parameters:[OCMArg any] success:[OCMArg any] failure:[OCMArg any]];
    
    [mockClient postPath:@"/sign-up" parameters:[[NSDictionary alloc] initWithObjectsAndKeys:@"123", @"client_id", @"tregg-user", @"username", @"1337", @"password", @"1337", @"confirm_password", nil] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *response = responseObject;
        
        // access_token verification
        XCTAssertEqual(@"token_123", [response valueForKey:@"access_token"], @"Access token is not the same");
        
        // save token
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:[response valueForKey:@"access_token"] forKey:kTFUserAccessToken];
        [userDefaults synchronize];

        XCTAssertEqual(@"token_123", [userDefaults objectForKey:kTFUserAccessToken], @"Saved access_token is not the same");
        
        // save user
        NSMutableDictionary *dictTreggUser = [[NSMutableDictionary alloc] init];
        
        [dictTreggUser setValue:[response valueForKey:@"user[id]"] forKey:@"id"];
        [dictTreggUser setValue:[response valueForKey:@"user[username]"] forKey:@"username"];
        [dictTreggUser setValue:[response valueForKey:@"user[password]"] forKey:@"password"];
        
        [userDefaults setValue:dictTreggUser forKey:kTFUserObjectKey];
        [userDefaults synchronize];
        
        XCTAssertEqual([dictTreggUser count], [[[NSDictionary alloc] initWithDictionary:[userDefaults objectForKey:kTFUserObjectKey]] count], @"Saved user is not the same");

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        XCTFail(@"Error during signUp");
    }];
}

- (void)testSignUpWithUsernameAlreadyTaken
{
    id mockClient = [OCMockObject mockForClass:[AFHTTPClient class]];
    
    [[mockClient expect] postPath:@"/sign-up" parameters:[OCMArg any] success:nil failure:[OCMArg checkWithBlock:^BOOL(void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error)) {
        failureBlock(nil, [NSError errorWithDomain:@"com.fungostudios.TreggiOS" code:402 userInfo:@{ NSLocalizedDescriptionKey : @"Username is already been taken."}]);
        
        return YES;
    }]];
    
    [mockClient postPath:@"/sign-up" parameters:[[NSDictionary alloc] initWithObjectsAndKeys:@"123", @"client_id", @"tregg-user", @"username", @"1337", @"password", @"1337", @"confirm_password", nil] success:nil
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        XCTAssertEqual(402, error.code, @"Error's code is not the same.");
        XCTAssertEqual(@"Username is already been taken.", error.localizedDescription, @"Error's description is not the same.");
    }];
}

- (void)testSignUpWithEmptyPassword
{
    id mockClient = [OCMockObject mockForClass:[AFHTTPClient class]];
    
    [[mockClient expect] postPath:@"/sign-up" parameters:[OCMArg any] success:nil failure:[OCMArg checkWithBlock:^BOOL(void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error)) {
        failureBlock(nil, [NSError errorWithDomain:@"com.fungostudios.TreggiOS" code:400 userInfo:@{ NSLocalizedDescriptionKey : @"Password is not valid."}]);
        return YES;
    }]];
    
    [mockClient postPath:@"/sign-up" parameters:[[NSDictionary alloc] initWithObjectsAndKeys:@"123", @"client_id", @"tregg-user", @"username", @"", @"password", @"", @"confirm_password", nil] success:nil
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    XCTAssertEqual(400, error.code, @"Error's code is not the same.");
                    XCTAssertEqual(@"Password is not valid.", error.localizedDescription, @"Error's description is not the same.");
                }];
}

- (void)testSignUpWithPasswordNoLongerThanSixChars
{
    id mockClient = [OCMockObject mockForClass:[AFHTTPClient class]];
    
    [[mockClient expect] postPath:@"/sign-up" parameters:[OCMArg any] success:nil failure:[OCMArg checkWithBlock:^BOOL(void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error)) {
        failureBlock(nil, [NSError errorWithDomain:@"com.fungostudios.TreggiOS" code:400 userInfo:@{ NSLocalizedDescriptionKey : @"Password is not valid."}]);
        return YES;
    }]];
    
    [mockClient postPath:@"/sign-up" parameters:[[NSDictionary alloc] initWithObjectsAndKeys:@"123", @"client_id", @"tregg-user", @"username", @"12345", @"password", @"12345", @"confirm_password", nil] success:nil
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    XCTAssertEqual(400, error.code, @"Error's code is not the same.");
                    XCTAssertEqual(@"Password is not valid.", error.localizedDescription, @"Error's description is not the same.");
                }];
}

@end
