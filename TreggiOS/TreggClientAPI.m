//
//  TreggSession.m
//  TreggiOS
//
//  Created by m3nTe on 1/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import "TreggClientAPI.h"

NSString *const kTFUrlApiConstant = @"http://localhost:1234";

@implementation TreggClientAPI

+ (TreggClientAPI *)sharedClient
{
    static TreggClientAPI *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[TreggClientAPI alloc] initWithBaseURL:[NSURL URLWithString:kTFUrlApiConstant]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)URL
{
    self = [super initWithBaseURL:URL];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    // Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
	[self setDefaultHeader:@"Accept" value:@"application/json"];
    
    return self;
}

- (TreggClientAPI *)authenticated
{
    // Best deal?
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [[TreggClientAPI sharedClient] setAuthorizationHeaderWithToken:[userDefaults objectForKey:kTFUserObjectKey]];

    return [TreggClientAPI sharedClient];
}

@end
