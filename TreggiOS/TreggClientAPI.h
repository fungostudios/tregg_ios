//
//  TreggSession.h
//  TreggiOS
//
//  Created by m3nTe on 1/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "TreggUser.h"

@interface TreggClientAPI : AFHTTPClient

+ (TreggClientAPI *)sharedClient;
- (TreggClientAPI *)authenticated;

@end
