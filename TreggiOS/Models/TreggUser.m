//
//  TreggUser.m
//  TreggiOS
//
//  Created by m3nTe on 1/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import "TreggUser.h"

@implementation TreggUser

static TreggUser *treggUser = nil;

@synthesize universalId = _universalId;
@synthesize userName = _userName;
@synthesize password = _password;
@synthesize facebookUid = _facebookUid;

NSString *const kTFUserObjectKey   = @"TreggUser";
NSString *const kTFUserAccessToken = @"TreggSessionToken";
NSString *const kTFLoginPath       = @"/oauth/authorize";
NSString *const kTFSignUpPath      = @"/sign-up";
NSString *const kTFMePath          = @"/me";

#pragma - TreggUser Public Methods

+ (void)signUp:(NSString *)userName
      password:(NSString *)password
      withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler
{
    TreggUser *user = [[TreggUser alloc] init];
    [user setUserName:userName];
    [user setPassword:password];
    
    [user signUp:^(TreggUser *user, NSError *error) {
       if (withCompletionHandler)
           withCompletionHandler(user, error);
    }];
}

- (void)signUp:(void(^)(TreggUser *user, NSError *error))withCompletionHandler
{
    NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
    [paramsDict setValue:[TreggConfig clientId] forKey:@"client_id"];
    [paramsDict setValue:self.userName forKey:@"username"];
    [paramsDict setValue:self.password forKey:@"password"];
    [paramsDict setValue:self.password forKey:@"confirm_password"];
    
    [[TreggClientAPI sharedClient] postPath:kTFSignUpPath parameters:paramsDict success:^(AFHTTPRequestOperation *operation, id responseJSON) {
        // we obtain token session and user
        [TreggUser saveTreggSecretsFromAPIResponse:responseJSON withCompletionHandler:^(TreggUser *user) {
            if (withCompletionHandler)
                withCompletionHandler(user, nil);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (withCompletionHandler)
            withCompletionHandler(nil, error);
    }];
}

+ (void)signIn:(NSString *)userName
      password:(NSString *)password
      withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler
{
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc] init];
    [dictParams setValue:[TreggConfig clientId] forKey:@"client_id"];
    [dictParams setValue:@"password" forKey:@"grant_type"];
    [dictParams setValue:userName forKey:@"username"];
    [dictParams setValue:password forKey:@"password"];
    
    [[TreggClientAPI sharedClient] postPath:kTFLoginPath parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseJSON) {
        // got only session token
        [self saveTreggSecretsFromAPIResponse:responseJSON withCompletionHandler:nil];
        
        [[[TreggClientAPI sharedClient] authenticated] getPath:kTFMePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseJSON) {
            // got only tregg user
            [self saveTreggSecretsFromAPIResponse:responseJSON withCompletionHandler:^(TreggUser *user) {
                if (withCompletionHandler)
                    withCompletionHandler(user, nil);
            }];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (withCompletionHandler)
                withCompletionHandler(nil, error);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (withCompletionHandler)
            withCompletionHandler(nil, error);
    }];
  
}

+ (void)signInWithFacebook:(NSString *)accessToken
     withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler
{
    [self signIn:Facebook accessToken:accessToken withCompletionHandler:withCompletionHandler];
}

- (void)signOut
{
    if ([TreggUser currentUser]) {
        // Clear OAuth Session Token
        [[TreggClientAPI sharedClient] clearAuthorizationHeader];
        
        // Clear all Tregg Data Saved
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:kTFUserObjectKey];
        [userDefaults removeObjectForKey:kTFUserAccessToken];
        [userDefaults synchronize];
    }else {
        // show a warning
    }
}

- (void)linkFacebook:(NSString *)accessToken
        withCompletionHandler:(void(^)(NSError *error))withCompletionHandler
{
    // TODO: call API for link user with Facebook.
}

- (void)unlinkFacebook:(void(^)(NSError *error))withCompletionHandler
{
    // TODO: call API for unlink user with Facebook.
}

+ (TreggUser *)currentUser
{
    // Saved TreggUser if not nil, is init just once.
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ((treggUser == nil) && ([userDefaults objectForKey:kTFUserObjectKey])) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            NSData *savedTreggUser = [userDefaults objectForKey:kTFUserObjectKey];
            NSDictionary *dictTreggUser = [NSKeyedUnarchiver unarchiveObjectWithData:savedTreggUser];
            treggUser = [TreggUser serializeTreggUser:dictTreggUser];
        });
    }
    
    return treggUser;
}

#pragma - TreggUser Private Methods

+ (TreggUser *)serializeTreggUser:(NSDictionary *)userData
{
    TreggUser *treggUser = [[TreggUser alloc] init];
    
    NSNumber *treggUserId = [NSNumber numberWithInt:[[userData valueForKey:@"id"] intValue]];
    NSString *treggUserUserName = [userData valueForKey:@"username"];

    if ([userData valueForKey:@"facebook_uid"])
        [treggUser setFacebookUid:[NSNumber numberWithInt:[[userData valueForKey:@"facebook_uid"] intValue]]];
    
    [treggUser setUniversalId:treggUserId];
    [treggUser setUserName:treggUserUserName];
    
    return treggUser;
}

+ (NSDictionary *)deserializeTreggUser:(TreggUser *)treggUser
{
    NSMutableDictionary *dictTreggUser = [[NSMutableDictionary alloc] init];
    
    [dictTreggUser setValue:treggUser.universalId forKey:@"id"];
    [dictTreggUser setValue:treggUser.userName forKey:@"username"];
    [dictTreggUser setValue:treggUser.facebookUid forKey:@"facebook_uid"];
    
    return dictTreggUser;
}

+ (void)saveTreggSessionTokenFromAPIResponse:(NSDictionary *)responseJSON
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[responseJSON valueForKey:@"access_token"] forKey:kTFUserAccessToken];
    [userDefaults synchronize];
}

+ (TreggUser *)saveTreggUserFromAPIResponse:(NSDictionary *)responseJSON
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    TreggUser *treggUser = [TreggUser serializeTreggUser:[responseJSON valueForKey:@"user"]];
    NSData *archivedTreggUser = [NSKeyedArchiver archivedDataWithRootObject:[TreggUser deserializeTreggUser:treggUser]];

    [userDefaults setObject:archivedTreggUser forKey:kTFUserObjectKey];
    [userDefaults synchronize];
    
    return treggUser;
}

+ (void)saveTreggSecretsFromAPIResponse:(NSDictionary *)responseJSON
                  withCompletionHandler:(void(^)(TreggUser *user))withCompletionHandler
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([responseJSON valueForKey:@"access_token"]) {
        [userDefaults setValue:[responseJSON valueForKey:@"access_token"] forKey:kTFUserAccessToken];
        [userDefaults synchronize];
    }
    
    if ([responseJSON valueForKey:@"user"]) {
        TreggUser *treggUser = [TreggUser serializeTreggUser:[responseJSON valueForKey:@"user"]];
        NSData *archivedTreggUser = [NSKeyedArchiver archivedDataWithRootObject:[TreggUser deserializeTreggUser:treggUser]];
        [userDefaults setObject:archivedTreggUser forKey:kTFUserObjectKey];
        [userDefaults synchronize];
        
        if (withCompletionHandler)
            withCompletionHandler(treggUser);
    }
}

+ (void)signIn:(providerType)provider
   accessToken:(NSString *)accessToken
   withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler
{
    switch (provider) {
        case Facebook:
        {
            NSMutableDictionary *dictParams = [[NSMutableDictionary alloc] init];
            [dictParams setValue:[TreggConfig clientId] forKey:@"client_id"];
            [dictParams setValue:@"assertion" forKey:@"grant_type"];
            [dictParams setValue:@"https://graph.facebook.com" forKey:@"assertion_type"];
            [dictParams setValue:accessToken forKey:@"assertion"];
            
            [[TreggClientAPI sharedClient] postPath:kTFLoginPath parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseJSON) {
                // got only session token
                [self saveTreggSecretsFromAPIResponse:responseJSON withCompletionHandler:nil];
                
                [[[TreggClientAPI sharedClient] authenticated] getPath:kTFMePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseJSON) {
                    // got only tregg user
                    [self saveTreggSecretsFromAPIResponse:responseJSON withCompletionHandler:^(TreggUser *user) {
                        if (withCompletionHandler)
                            withCompletionHandler(user, nil);
                    }];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if (withCompletionHandler)
                        withCompletionHandler(nil, error);
                }];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                if (withCompletionHandler)
                    withCompletionHandler(nil, error);
            }];
            
            break;
        }
        default:
            break;
    }
}

@end
