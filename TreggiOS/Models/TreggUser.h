//
//  TreggUser.h
//  TreggiOS
//
//  Created by m3nTe on 1/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TreggClientAPI.h"
#import "TreggConfig.h"

typedef enum {
    Facebook
} providerType;

@interface TreggUser : NSObject

@property (nonatomic, strong) NSNumber *universalId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSNumber *facebookUid;

FOUNDATION_EXPORT NSString *const kTFUserObjectKey;
FOUNDATION_EXPORT NSString *const kTFUserAccessToken;
FOUNDATION_EXPORT NSString *const kTFLoginPath;
FOUNDATION_EXPORT NSString *const kTFSignUpPath;

+ (void)signUp:(NSString *)userName
      password:(NSString *)password
      withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler;

- (void)signUp:(void(^)(TreggUser *user, NSError *error))withCompletionHandler;

+ (void)signIn:(NSString *)userName
      password:(NSString *)password
      withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler;
+ (void)signInWithFacebook:(NSString *)accessToken
     withCompletionHandler:(void(^)(TreggUser *user, NSError *error))withCompletionHandler;

- (void)signOut;

- (void)linkFacebook:(NSString *)accessToken
        withCompletionHandler:(void(^)(NSError *error))withCompletionHandler;
- (void)unlinkFacebook:(void(^)(NSError *error))withCompletionHandler;

+ (TreggUser *)currentUser;

@end