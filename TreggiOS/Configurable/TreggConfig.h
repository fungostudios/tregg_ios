//
//  TreggConfig.h
//  TreggiOS
//
//  Created by m3nTe on 8/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TreggConfig : NSObject

// Getters
+ (NSString *)clientId;
+ (NSString *)baseURL;
// Setters
+ (void)setClientId:(NSString *)value;
+ (void)setBaseURL:(NSString *)value;

@end
