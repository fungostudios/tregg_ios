//
//  TreggConfig.m
//  TreggiOS
//
//  Created by m3nTe on 8/10/13.
//  Copyright (c) 2013 Fungo Studios. All rights reserved.
//

#import "TreggConfig.h"

@implementation TreggConfig

static NSString *clientId = nil;
static NSString *baseURL  = nil;

+ (NSString *)clientId
{
    return clientId;
}

+ (void)setClientId:(NSString *)value
{
    clientId = value;
}

+ (NSString *)baseURL
{
    return baseURL;
}

+ (void)setBaseURL:(NSString *)value
{
    baseURL = value;
}

@end
